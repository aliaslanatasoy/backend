package com.aslan.energyconsumptionbackend.validations;

import com.aslan.energyconsumptionbackend.common.FaultType;
import com.aslan.energyconsumptionbackend.entity.Fraction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by aslan.atasoy on 11/12/2017.
 */
@Component
public class FractionValidation {

    /*
    * This class is created in order to validate fractions
    * Any required fraction validations have to implemented in
    * this class. ALl return types should be ResponseEntity for validations
    * */
    public ResponseEntity validateFraction(List<Fraction> fractionList) {
        /*
        * Incoming request are grouped according to its profiles
        * */
        Map<String, BigDecimal> processFractionMap = new HashMap<>();

        for (Fraction fractionItem : fractionList) {
            if (processFractionMap.get(fractionItem.getMonthProfile().getProfile()) != null) {
                processFractionMap.put(fractionItem.getMonthProfile().getProfile(),
                        processFractionMap.get(fractionItem.getMonthProfile().getProfile()).add(fractionItem.getFractions()) );
            } else {
                processFractionMap.put(fractionItem.getMonthProfile().getProfile(), fractionItem.getFractions());
            }
        }
        /*
        * For each profiles that grouped before, checks
        * summation of all fractions should exactly equals 1
        * and then for each profile, all months should be exists.
        * If everything is ok then returns response as validated,
        * if not returns fault type as response entity
        * */
        for (Map.Entry<String, BigDecimal> mapEntry : processFractionMap.entrySet()) {
            if (mapEntry.getValue().compareTo(new BigDecimal("1.00")) != 0) {
                FaultType fault = new FaultType();
                fault.setFaultCode(HttpStatus.BAD_REQUEST.toString());
                fault.setFaultMessage("Sum of all fractions should be 1, validation failed for profile: " + mapEntry.getKey());
                return new ResponseEntity(fault, HttpStatus.BAD_REQUEST);
            }

            List<Fraction> tempList = fractionList.stream().filter(fraction -> mapEntry.getKey()
                    .equals(fraction.getMonthProfile().getProfile())).collect(Collectors.toList());

            for (String month : new DateFormatSymbols().getShortMonths()) {
                String temp = month.toUpperCase();
                if((!month.isEmpty() && tempList.stream().filter(fraction -> temp.equals(fraction.getMonthProfile().getMonth())).count() != 1)){
                    FaultType fault = new FaultType();
                    fault.setFaultCode(HttpStatus.BAD_REQUEST.toString());
                    fault.setFaultMessage("Input data always contains ratios for the twelve months for a given profile: "
                            + tempList.get(0).getMonthProfile().getProfile());
                    return new ResponseEntity(fault,HttpStatus.BAD_REQUEST);
                }
            }


        }

        return ResponseEntity.ok().body(fractionList);
    }

}
