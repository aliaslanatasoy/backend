package com.aslan.energyconsumptionbackend.validations;

import com.aslan.energyconsumptionbackend.common.FaultType;
import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;
import com.aslan.energyconsumptionbackend.entity.Fraction;
import com.aslan.energyconsumptionbackend.entity.MeterReading;
import com.aslan.energyconsumptionbackend.repository.FractionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by aslan.atasoy on 11/14/2017.
 */
@Component
public class MeterReadingValidation {

    @Autowired
    private FractionRepository fractionRepository;
    /*
       * This class is created in order to validate meter readings
       * Any required meter reading validations have to implemented in
       * this class. All return types should be in ResponseEntity type
       * for validations
       * */
    public ResponseEntity validateMeterReading(List<MeterReading> meterReadingList) throws Exception {
        /*
        * Method groups given meter readings as their connectionId
        *
        * */
        Map<Long, List<MeterReading>> processedMeterReading = new HashMap<>();
        for (MeterReading meterReading : meterReadingList) {
            if (processedMeterReading.get(meterReading.getMonthProfileConnection().getConnection()) != null) {
                processedMeterReading.get(meterReading.getMonthProfileConnection().getConnection()).add(meterReading);
            } else {
                List<MeterReading> newMeterReadingItem = new ArrayList<>();
                newMeterReadingItem.add(meterReading);
                processedMeterReading.put(meterReading.getMonthProfileConnection().getConnection(), newMeterReadingItem);
            }
        }
        /*
        * After groping request by connectionId, method checks
        * all months exists for each connection.
        * After that, method checks there is a fraction entry for
         * given profile month by month and then checks the meter reading
         * should not be lower than before months meter reading.
         *
        * */
        for (Map.Entry<Long, List<MeterReading>> entry : processedMeterReading.entrySet()) {
            int totalMeterForAConnection = 0;
            List<MeterReading> currentMeterReadingList = entry.getValue();
            for (String month : new DateFormatSymbols().getShortMonths()) {
                String upperCaseMonth = month.toUpperCase();
                if(!month.isEmpty()){
                    if (entry.getValue().stream().filter(meterReading ->
                            upperCaseMonth.equals(meterReading.getMonthProfileConnection().getMonth())).count() != 1) {
                        FaultType faultType = new FaultType();
                        faultType.setFaultCode(HttpStatus.BAD_REQUEST.toString());
                        faultType.setFaultMessage("A connection should have meter reading for 12 months. Connection " + entry.getKey()
                                + " do not have a value for " + upperCaseMonth);
                        return new ResponseEntity(faultType, HttpStatus.BAD_REQUEST);
                    } else {
                        Optional<MeterReading> optMeterReadingForCurrentMonth = currentMeterReadingList.stream().filter(meterReading -> upperCaseMonth.equals(meterReading.getMonthProfileConnection().getMonth())).findFirst();
                        MeterReading meterReadingForCurrentMonth = optMeterReadingForCurrentMonth.get();

                        Fraction currentFraction = fractionRepository.
                                findOne(new MonthProfile(meterReadingForCurrentMonth.getMonthProfileConnection().getMonth(),
                                        meterReadingForCurrentMonth.getMonthProfileConnection().getProfile()));

                        if (currentFraction == null) {
                            FaultType faultType = new FaultType();
                            faultType.setFaultCode(HttpStatus.NOT_FOUND.toString());
                            faultType.setFaultMessage("Fractions for the profiles contained in the data should exist, fraction value for "
                                    + meterReadingForCurrentMonth.getMonthProfileConnection().toString() + "not found");
                            return new ResponseEntity(faultType, HttpStatus.NOT_FOUND);
                        } else if (totalMeterForAConnection > meterReadingForCurrentMonth.getMeterReading() && !upperCaseMonth.equals("JAN")) {
                            FaultType faultType = new FaultType();
                            faultType.setFaultCode(HttpStatus.BAD_REQUEST.toString());
                            faultType.setFaultMessage("A meter reading for a month " + upperCaseMonth + "should not be lower than the previous month.");
                            return new ResponseEntity(faultType, HttpStatus.BAD_REQUEST);
                        } else {
                            totalMeterForAConnection = meterReadingForCurrentMonth.getMeterReading();
                        }
                    }
                }

            }
            /*
            * This part of code is calculating the appropriate meter reading values for
            * months and checks if data is valid. If data is not valid returns fault with calculated
            * meter readings.
            * */
            int previousMeterReading = 0;
            for (String month : new DateFormatSymbols().getShortMonths()) {
                String upperCaseMonth = month.toUpperCase();
                if (!upperCaseMonth.isEmpty()) {
                    Optional<MeterReading> optMeterReadingForCurrentMonth = currentMeterReadingList.stream().filter(meterReading -> upperCaseMonth.equals(meterReading.getMonthProfileConnection().getMonth())).findFirst();
                    MeterReading meterReadingItem = optMeterReadingForCurrentMonth.get();
                    BigDecimal retrievedFraction = fractionRepository.findOne(new MonthProfile(meterReadingItem.getMonthProfileConnection().getMonth(),
                            meterReadingItem.getMonthProfileConnection().getProfile())).getFractions();

                    int currentMeterReading = meterReadingItem.getMeterReading();
                    float expectedConsumption = totalMeterForAConnection * retrievedFraction.floatValue();
                    int currentConsumptions = currentMeterReading - previousMeterReading;

                    if ((currentConsumptions < expectedConsumption - (expectedConsumption * 0.25)) || (currentConsumptions > expectedConsumption + (expectedConsumption * 0.25))) {
                        FaultType faultType = new FaultType();
                        faultType.setFaultCode(HttpStatus.BAD_REQUEST.toString());
                        faultType.setFaultMessage("Consumption for " + meterReadingItem.getMonthProfileConnection().toString() +
                                "should be between " + (expectedConsumption - (expectedConsumption * 0.25)) + "and " + (expectedConsumption + (expectedConsumption * 0.25)));
                        return new ResponseEntity(faultType,HttpStatus.BAD_REQUEST);
                    }
                    previousMeterReading = currentMeterReading;
                }
            }
        }
        return ResponseEntity.ok().body(meterReadingList);
    }
}
