package com.aslan.energyconsumptionbackend.dataSpecs;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by aslan.atasoy on 11/12/2017.
 * MonthProfile data specification for Fraction objects
 */
@Embeddable
public class MonthProfile implements Serializable{

    private String month;
    private String profile;

    public MonthProfile() {
    }

    public MonthProfile(String month, String profile) {
        this.month = month;
        this.profile = profile;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
