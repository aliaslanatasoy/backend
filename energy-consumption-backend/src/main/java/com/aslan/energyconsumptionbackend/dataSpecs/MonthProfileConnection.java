package com.aslan.energyconsumptionbackend.dataSpecs;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by aslan.atasoy on 11/14/2017.
 * Month profile and connection data specification
 * for Meter reading objects
 */
@Embeddable
public class MonthProfileConnection implements Serializable{
    private String month;
    private String profile;
    private long connection;

    public MonthProfileConnection() {
    }

    public MonthProfileConnection(String month, String profile,long connection) {
        this.month = month;
        this.profile = profile;
        this.connection = connection;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public long getConnection() {
        return connection;
    }

    public void setConnection(long connection) {
        this.connection = connection;
    }
}
