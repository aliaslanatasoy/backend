package com.aslan.energyconsumptionbackend.controller;

import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;
import com.aslan.energyconsumptionbackend.entity.Fraction;
import com.aslan.energyconsumptionbackend.service.serviceImpl.FractionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by aslan.atasoy on 11/10/2017.
 */
@RestController
@RequestMapping(value = "/fraction")
public class FractionController {

    @Autowired
    private FractionServiceImpl fractionService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllFractions() throws Exception{
        return fractionService.getAllFractions();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/{profile}")
    public ResponseEntity getFractionByProfile(@PathVariable String profile) throws Exception{
        return (ResponseEntity) fractionService.getFractionByProfile(profile);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createFraction(@RequestBody List<Fraction> fractions)throws Exception{
        return fractionService.createFraction(fractions);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity updateFraction(@RequestBody List<Fraction> fractionList) throws Exception {
        return fractionService.updateFraction(fractionList);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/{profile}")
    public ResponseEntity deleteFraction(@PathVariable String profile) throws Exception {
        return fractionService.deleteFraction(profile);
    }
}
