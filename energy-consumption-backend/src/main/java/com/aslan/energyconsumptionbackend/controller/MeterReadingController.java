package com.aslan.energyconsumptionbackend.controller;

import com.aslan.energyconsumptionbackend.entity.MeterReading;
import com.aslan.energyconsumptionbackend.service.serviceImpl.MeterReadingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by aslan.atasoy on 11/10/2017.
 */
@RestController
@RequestMapping(value = "/meter-reading")
public class MeterReadingController {
    @Autowired
    private MeterReadingServiceImpl meterReadingService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllMeterReadings(){
        return meterReadingService.getAllMeterReadings();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/month/{month}")
    public ResponseEntity getMeterReadingByMonth(@PathVariable String month)throws Exception{
        return meterReadingService.getMeterReadingByMonth(month);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/profile/{profile}")
    public ResponseEntity getMeterReadingByProfile(@PathVariable String profile){
        return meterReadingService.getMeterReadingByProfile(profile);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createMeterReading(@RequestBody List<MeterReading> meterReadingList) throws Exception {
        return meterReadingService.createMeterReading(meterReadingList);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity updateMeterReading(@RequestBody  List<MeterReading> meterReadings) throws Exception{
        return meterReadingService.updateMeterReading(meterReadings);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "{profile}")
    public ResponseEntity deleteMeterReading(@PathVariable String profile){
        return meterReadingService.deleteMeterReading(profile);
    }
}
