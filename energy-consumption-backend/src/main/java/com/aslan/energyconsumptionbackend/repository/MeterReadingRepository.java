package com.aslan.energyconsumptionbackend.repository;

import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;
import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfileConnection;
import com.aslan.energyconsumptionbackend.entity.MeterReading;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aslan.atasoy on 11/09/2017.
 */
@Repository
public interface MeterReadingRepository extends JpaRepository<MeterReading,MonthProfileConnection> {
    List<MeterReading> findByMonthProfileConnection_Month(String month);
    List<MeterReading> findByMonthProfileConnection_Profile(String profile);
    void deleteMeterReadingByMonthProfileConnection_Profile(String profile);
}
