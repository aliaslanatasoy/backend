package com.aslan.energyconsumptionbackend.repository;

import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;
import com.aslan.energyconsumptionbackend.entity.Fraction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aslan.atasoy on 11/09/2017.
 */
@Repository
public interface FractionRepository extends JpaRepository<Fraction,MonthProfile> {

}
