package com.aslan.energyconsumptionbackend.entity;

import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;
import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfileConnection;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by aslan.atasoy on 11/09/2017.
 */
@Entity
@Table(name = "METER_READING")
public class MeterReading implements Serializable{

    private MonthProfileConnection monthProfileConnection;
    private int meterReading;

    @EmbeddedId
    public MonthProfileConnection getMonthProfileConnection() {
        return monthProfileConnection;
    }

    public void setMonthProfileConnection(MonthProfileConnection monthProfileConnection) {
        this.monthProfileConnection = monthProfileConnection;
    }


    public int getMeterReading() {
        return meterReading;
    }

    public void setMeterReading(int meterReading) {
        this.meterReading = meterReading;
    }
}
