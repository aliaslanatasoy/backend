package com.aslan.energyconsumptionbackend.entity;

import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by aslan.atasoy on 11/09/2017.
 */
@Entity
public class Fraction implements Serializable {

    private MonthProfile monthProfile;
    private BigDecimal fractions;

    @EmbeddedId
    public MonthProfile getMonthProfile() {
        return monthProfile;
    }

    public void setMonthProfile(MonthProfile monthProfile) {
        this.monthProfile = monthProfile;
    }

    public BigDecimal getFractions() {
        return fractions;
    }

    public void setFractions(BigDecimal fractions) {
        this.fractions = fractions;
    }
}
