package com.aslan.energyconsumptionbackend.service.serviceImpl;

import com.aslan.energyconsumptionbackend.common.FaultType;
import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;
import com.aslan.energyconsumptionbackend.entity.Fraction;
import com.aslan.energyconsumptionbackend.repository.FractionRepository;
import com.aslan.energyconsumptionbackend.service.serviceInterfaces.FractionService;
import com.aslan.energyconsumptionbackend.validations.FractionValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aslan.atasoy on 11/09/2017.
 */
@Service
public class FractionServiceImpl implements FractionService {
    @Autowired
    private FractionRepository fractionRepository;

    @Autowired
    private FractionValidation fractionValidation;

    /*
    * In order to get all fractions from database,
    * if there is no record for fraction service will return
    * fault as nothing found..
    * */
    @Override
    @Transactional(readOnly = true)
    public ResponseEntity getAllFractions() throws Exception {
        List<Fraction> response = fractionRepository.findAll();
        if (response != null && !response.isEmpty() && response.size() != 0) {
            return ResponseEntity.ok().body(response);
        } else {
            FaultType fault = new FaultType();
            fault.setFaultMessage("Nothing found");
            fault.setFaultCode(HttpStatus.NOT_FOUND.toString());
            return new ResponseEntity(fault, HttpStatus.NOT_FOUND);
        }

    }

    /*
    * In order to get fraction by profile from database
    * String profile is must to retrieve related data
    * Method first check if data exists in the database
    * if no data found related with given profile
    * then returns fault
    * */
    @Override
    @Transactional(readOnly = true)
    public ResponseEntity getFractionByProfile(String profile) throws Exception {
        List<Fraction> responseList = new ArrayList<>();

        for (String month : new DateFormatSymbols().getShortMonths()) {
            Fraction fraction = fractionRepository.findOne(new MonthProfile(month.toUpperCase(), profile));
            if (fraction != null) {
                responseList.add(fraction);
            }
        }

        if (!responseList.isEmpty()) {
            return ResponseEntity.ok().body(responseList);
        } else {
            FaultType fault = new FaultType();
            fault.setFaultMessage("Nothing found with given profile " + profile);
            fault.setFaultCode(HttpStatus.NOT_FOUND.toString());
            return new ResponseEntity(fault, HttpStatus.NOT_FOUND);
        }

    }

    /**
     * In order to create fraction in database.
     * Method first validates the response and
     * if validation is succesful then checks
     * if any data written before with that MontProfile spec
     * if data saved before method returns conflict and do not allow
     * to push data to database.If there is no data created before
     * request send to repository to save data in database.
     */
    @Override
    @Transactional
    public ResponseEntity createFraction(List<Fraction> fractionList) throws Exception {
        ResponseEntity response = fractionValidation.validateFraction(fractionList);
        if (response.getStatusCode() != HttpStatus.OK) {
            return response;
        }

        for (Fraction fraction : fractionList) {
            Fraction retrievedFraction = fractionRepository.findOne(fraction.getMonthProfile());
            if (retrievedFraction != null && !retrievedFraction.equals(fraction)) {
                FaultType faultType = new FaultType();
                faultType.setFaultCode(HttpStatus.CONFLICT.toString());
                faultType.setFaultMessage("Data already created before");
                return new ResponseEntity(faultType, HttpStatus.CONFLICT);
            }
        }

        return new ResponseEntity(fractionRepository.save(fractionList), HttpStatus.CREATED);

    }
    /*
    * In order to update given request to database
    * Request must be validated before going update phase
    * If request is valid, then not checking any data update,
    * directly saves the sent data. Additionally,
    * If there is no data found in database method returns fault,
    * no data data to update.
    * */
    @Override
    @Transactional
    public ResponseEntity updateFraction(List<Fraction> fractionList) throws Exception {
        ResponseEntity response = fractionValidation.validateFraction(fractionList);
        if (response.getStatusCode() != HttpStatus.OK) {
            return response;
        }

        List<Fraction> retrievedFractionList = new ArrayList<>();
        for (Fraction fraction : fractionList) {
            Fraction retrievedFraction = fractionRepository.findOne(fraction.getMonthProfile());
            if (retrievedFraction != null) {
                retrievedFractionList.add(retrievedFraction);
            }
        }

        if (retrievedFractionList.size() != fractionList.size()) {
            FaultType faultType = new FaultType();
            faultType.setFaultCode(HttpStatus.BAD_REQUEST.toString());
            faultType.setFaultMessage("Requested data does not match to saved data!");
            return new ResponseEntity(faultType, HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok().body(fractionRepository.save(fractionList));
    }

    /*
    * In order to delete data from database.
    * Method retrieves data at first and if
    * there is no data with given profile
    * returns fault with no data found.
    * */
    @Override
    @Transactional
    public ResponseEntity deleteFraction(String profile) throws Exception {
        List<Fraction> fractionList = new ArrayList<>();
        for (String month : new DateFormatSymbols().getShortMonths()) {
            Fraction fraction = fractionRepository.findOne(new MonthProfile(month.toUpperCase(), profile));
            if (fraction != null)
                fractionList.add(fraction);
        }

        if (!fractionList.isEmpty()) {
            fractionRepository.delete(fractionList);
            return new ResponseEntity("All data deleted with given profile " + profile, HttpStatus.OK);
        }

        return new ResponseEntity(new FaultType("No data found with profile " + profile, HttpStatus.NOT_FOUND.toString()), HttpStatus.NOT_FOUND);
    }
}
