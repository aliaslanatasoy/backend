package com.aslan.energyconsumptionbackend.service.serviceInterfaces;

import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfileConnection;
import com.aslan.energyconsumptionbackend.entity.MeterReading;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by aslan.atasoy on 11/09/2017.
 */

public interface MeterReadingService {
    ResponseEntity getAllMeterReadings()throws Exception;
    ResponseEntity getMeterReadingByProfile(String profile)throws Exception;
    ResponseEntity getMeterReadingByMonth(String month)throws Exception;
    ResponseEntity createMeterReading(List<MeterReading> meterReadings)throws Exception;
    ResponseEntity updateMeterReading(List<MeterReading> meterReadings)throws Exception;
    ResponseEntity deleteMeterReading(String profile)throws Exception;
}
