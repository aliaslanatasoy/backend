package com.aslan.energyconsumptionbackend.service.serviceInterfaces;

import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;
import com.aslan.energyconsumptionbackend.entity.Fraction;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by aslan.atasoy on 11/09/2017.
 */

public interface FractionService {
    ResponseEntity getAllFractions() throws Exception;
    ResponseEntity getFractionByProfile(String profile) throws Exception;
    ResponseEntity createFraction(List<Fraction> fractionList) throws Exception;
    ResponseEntity updateFraction(List<Fraction> fractionList) throws Exception;
    ResponseEntity deleteFraction(String profile)throws Exception;
}
