package com.aslan.energyconsumptionbackend.service.serviceImpl;

import com.aslan.energyconsumptionbackend.common.FaultType;
import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;
import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfileConnection;
import com.aslan.energyconsumptionbackend.entity.MeterReading;
import com.aslan.energyconsumptionbackend.repository.MeterReadingRepository;
import com.aslan.energyconsumptionbackend.service.serviceInterfaces.MeterReadingService;
import com.aslan.energyconsumptionbackend.validations.MeterReadingValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aslan.atasoy on 11/09/2017.
 */
@Service
public class MeterReadingServiceImpl implements MeterReadingService {

    @Autowired
    private MeterReadingRepository meterReadingRepository;

    @Autowired
    private MeterReadingValidation meterReadingValidation;

    /*
    * In order to retrieve all meter readings from database
    * Method checks if any data is stored before, If not,
    * then returns fault as nothing found.
    * */
    @Override
    @Transactional(readOnly = true)
    public ResponseEntity getAllMeterReadings() {
        List<MeterReading> response = meterReadingRepository.findAll();
        if(!response.isEmpty() && response.size() != 0 && response != null){
            return ResponseEntity.ok().body(response);
        }

        FaultType faultType = new FaultType();
        faultType.setFaultMessage("Nothing found for meter reading");
        faultType.setFaultCode(HttpStatus.NOT_FOUND.toString());

        return new ResponseEntity(faultType,HttpStatus.NOT_FOUND);
    }
    /*
    * In order to retrieve data with given profile
    * If no data exists with given profile method returns fault
    *
    * */
    @Override
    @Transactional(readOnly = true)
    public ResponseEntity getMeterReadingByProfile(String profile) {
        List<MeterReading> response = meterReadingRepository.findByMonthProfileConnection_Profile(profile);
        if(response != null && response.size() != 0 && !response.isEmpty()){
            return ResponseEntity.ok().body(response);
        }
        FaultType faultType = new FaultType();
        faultType.setFaultCode(HttpStatus.NOT_FOUND.toString());
        faultType.setFaultMessage("Nothing found for profile "+ profile);
        return new ResponseEntity(faultType,HttpStatus.NOT_FOUND);
    }
    /*
    * In order to retrieve data with given month in the form of
    * three letter java month.
    * if nothing found with given month method returns nothing found.
    * */
    @Override
    public ResponseEntity getMeterReadingByMonth(String month) throws Exception {
        List<MeterReading> response = meterReadingRepository.findByMonthProfileConnection_Month(month);
        if(response != null && response.size() != 0 && !response.isEmpty()){
            return ResponseEntity.ok().body(response);
        }
        FaultType faultType = new FaultType();
        faultType.setFaultCode(HttpStatus.NOT_FOUND.toString());
        faultType.setFaultMessage("Nothing found for month "+ month);
        return new ResponseEntity(faultType,HttpStatus.NOT_FOUND);
    }
    /*
    * In order to create meter reading in database,
    * method first validates the request, if request is not valid
    * method returns fault. If data already created before method
    * returns conflict as data created before.
    * */
    @Override
    public ResponseEntity createMeterReading(List<MeterReading> meterReadings) throws Exception{
        ResponseEntity response = meterReadingValidation.validateMeterReading(meterReadings);
        if(response.getStatusCode() != HttpStatus.OK){
            return response;
        }

        for(MeterReading meterReading:meterReadings){
            MeterReading retrievedMeterReading = meterReadingRepository.findOne(meterReading.getMonthProfileConnection());
            if(retrievedMeterReading != null){
                FaultType faultType = new FaultType();
                faultType.setFaultCode(HttpStatus.CONFLICT.toString());
                faultType.setFaultMessage("Data already created before");
                return new ResponseEntity(faultType,HttpStatus.CONFLICT);
            }
        }
        return new ResponseEntity(meterReadingRepository.save(meterReadings),HttpStatus.CREATED);
    }
    /*
    * In order to update meter readings,
    * Method validates the response and if response is not valid
    * returns fault. Additionally, If sent request is not found in database
    * method returns fault as there is no data found to update
    * */
    @Override
    public ResponseEntity updateMeterReading(List<MeterReading> meterReadings) throws Exception{
        ResponseEntity validatedResponse = meterReadingValidation.validateMeterReading(meterReadings);
        if(validatedResponse.getStatusCode() != HttpStatus.OK) {
            return validatedResponse;
        }
        for(MeterReading meterReading:meterReadings){
            MeterReading retrievedMeterReading = meterReadingRepository.findOne(meterReading.getMonthProfileConnection());
            if(retrievedMeterReading == null){
                FaultType faultType = new FaultType();
                faultType.setFaultCode(HttpStatus.BAD_REQUEST.toString());
                faultType.setFaultMessage("Requested data does not match to saved data!");
                return new ResponseEntity(faultType,HttpStatus.BAD_REQUEST);
            }
        }
        return ResponseEntity.ok().body(meterReadings);
    }
    /*
    * In order to delete meter reading from database with profile. Method
    * checks if data is exist in database, if data do not exists it returns fault
    * as nothing found. If data exists then deletes data and returns corresponding message.
    * */
    @Override
    @Transactional
    public ResponseEntity deleteMeterReading(String profile) {
        List<MeterReading> retrievedMeterReadings = meterReadingRepository.findByMonthProfileConnection_Profile(profile);
        if(retrievedMeterReadings.isEmpty() || retrievedMeterReadings.size() == 0){
            FaultType faultType = new FaultType();
            faultType.setFaultCode(HttpStatus.BAD_REQUEST.toString());
            faultType.setFaultMessage("No Meter Readings found with the given profile: " + profile);
            return new ResponseEntity(faultType,HttpStatus.BAD_REQUEST);
        }

        meterReadingRepository.deleteMeterReadingByMonthProfileConnection_Profile(profile);
        return new ResponseEntity("All data deleted with given profile " + profile, HttpStatus.OK);
    }
}
