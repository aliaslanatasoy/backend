package com.aslan.energyconsumptionbackend.common;

import java.io.Serializable;

/**
 * Created by aslan.atasoy on 11/10/2017.
 * Common fault type to return when fault occurs
 * in the code..
 */
public class FaultType implements Serializable{
    private String faultMessage;
    private String faultCode;

    public FaultType() {
    }

    public FaultType(String faultMessage, String faultCode) {
        this.faultMessage = faultMessage;
        this.faultCode = faultCode;
    }

    public String getFaultMessage() {
        return faultMessage;
    }

    public void setFaultMessage(String faultMessage) {
        this.faultMessage = faultMessage;
    }

    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }
}
