package com.aslan.energyconsumptionbackend.meterReading;

import com.aslan.energyconsumptionbackend.common.DataInitializer;
import com.aslan.energyconsumptionbackend.common.FaultType;
import com.aslan.energyconsumptionbackend.controller.MeterReadingController;
import com.aslan.energyconsumptionbackend.service.serviceImpl.MeterReadingServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Created by aslan.atasoy on 11/21/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(value = MeterReadingController.class, secure = false)
public class MeterReadingTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private MeterReadingServiceImpl meterReadingService;

    private DataInitializer dataInitializer = new DataInitializer();

    @Test
    public void testGetAllMeterReadings()throws Exception{
        Mockito.when(meterReadingService.getAllMeterReadings())
                .thenReturn(new ResponseEntity(dataInitializer.getMeterReading("A"), HttpStatus.OK));
        mockMvc.perform(MockMvcRequestBuilders.get("/meter-reading").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$",hasSize(12)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].monthProfileConnection.profile").value("A"))
                .andReturn();

    }

    @Test
    public void testGetMeterReadingByProfile()throws Exception{
        Mockito.when(meterReadingService.getAllMeterReadings())
                .thenReturn(new ResponseEntity(dataInitializer.getMeterReading("B"), HttpStatus.OK));

        mockMvc.perform(MockMvcRequestBuilders.get("/meter-reading").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].monthProfileConnection.profile").value("B"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(12)))
                .andReturn();
    }

    @Test
    public void testUpdateMeterReading() throws Exception{
        Mockito.when(meterReadingService.updateMeterReading(Mockito.anyList()))
                .thenReturn(new ResponseEntity(dataInitializer.getMeterReading("B"), HttpStatus.OK));

        mockMvc.perform(MockMvcRequestBuilders.put("/meter-reading").contentType(MediaType.APPLICATION_JSON)
                .content(dataInitializer.getMeterReadingB()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(12)))
                .andExpect(jsonPath("$.[0].monthProfileConnection.profile").value("B"))
                .andReturn();
    }

    @Test
    public void testCreateMeterReading() throws Exception{
        Mockito.when(meterReadingService.createMeterReading(Mockito.anyList()))
                .thenReturn(new ResponseEntity(dataInitializer.getMeterReading("A"), HttpStatus.CREATED));

        mockMvc.perform(MockMvcRequestBuilders.post("/meter-reading").contentType(MediaType.APPLICATION_JSON)
                .content(dataInitializer.getMeterReadingA()))
                .andExpect(jsonPath("$",hasSize(12)))
                .andExpect(jsonPath("$.[0].monthProfileConnection.profile").value("A"))
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void getMeterReadingsWith404() throws Exception{
        FaultType faultType = new FaultType();
        faultType.setFaultMessage("Nothing found for meter reading");
        faultType.setFaultCode(HttpStatus.NOT_FOUND.toString());
        Mockito.when(meterReadingService.getAllMeterReadings())
                .thenReturn(new ResponseEntity(faultType, HttpStatus.NOT_FOUND));

        mockMvc.perform(MockMvcRequestBuilders.get("/meter-reading"))
                .andExpect(jsonPath("$.faultMessage").value("Nothing found for meter reading"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void getMeterReadingWithProfileWith404() throws Exception{
        FaultType faultType = new FaultType();
        faultType.setFaultCode(HttpStatus.NOT_FOUND.toString());
        faultType.setFaultMessage("Nothing found for profile "+ "A");
        Mockito.when(meterReadingService.getMeterReadingByProfile("A"))
                .thenReturn(new ResponseEntity(faultType, HttpStatus.NOT_FOUND));

        mockMvc.perform(MockMvcRequestBuilders.get("/meter-reading/profile/{profile}","A").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.faultMessage").value("Nothing found for profile A"))
                .andReturn();
    }

}
