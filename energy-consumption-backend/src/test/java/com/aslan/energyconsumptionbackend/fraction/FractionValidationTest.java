package com.aslan.energyconsumptionbackend.fraction;

import com.aslan.energyconsumptionbackend.common.DataInitializer;
import com.aslan.energyconsumptionbackend.common.FaultType;
import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;
import com.aslan.energyconsumptionbackend.entity.Fraction;
import com.aslan.energyconsumptionbackend.validations.FractionValidation;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * Created by aslan.atasoy on 11/20/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(value = FractionValidation.class,secure = false)
public class FractionValidationTest {

    @Autowired
    private FractionValidation fractionValidation;
    private DataInitializer dataInitializer = new DataInitializer();

    @Test
    public void testValidateFraction() throws Exception{
        List<Fraction> fractionList = dataInitializer.getFraction("A");
        ResponseEntity responseEntity = fractionValidation.validateFraction(fractionList);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody(),fractionList);
    }

    @Test
    public void testValidateFractionWithMissingMonth()throws Exception{
        List<Fraction> fractionList = dataInitializer.getFraction("B");
        fractionList.get(0).setMonthProfile(new MonthProfile(null,"B"));

        ResponseEntity responseEntity = fractionValidation.validateFraction(fractionList);
        FaultType faultType = (FaultType)responseEntity.getBody();

        assertEquals(responseEntity.getStatusCode(),HttpStatus.BAD_REQUEST);
        assertEquals(faultType.getFaultMessage(),"Input data always contains ratios for the twelve months for a given profile: B");
    }

    @Test
    public void testValidateFractionWithWrongTotalFraction()throws Exception{
        List<Fraction> fractionList = dataInitializer.getFraction("A");
        fractionList.get(0).setFractions(BigDecimal.valueOf(10));

        ResponseEntity responseEntity = fractionValidation.validateFraction(fractionList);
        FaultType faultType = (FaultType)responseEntity.getBody();

        assertEquals(responseEntity.getStatusCode(),HttpStatus.BAD_REQUEST);
        assertEquals(faultType.getFaultMessage(),"Sum of all fractions should be 1, validation failed for profile: A");
    }

}
