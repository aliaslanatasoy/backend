package com.aslan.energyconsumptionbackend.fraction;

import com.aslan.energyconsumptionbackend.common.DataInitializer;
import com.aslan.energyconsumptionbackend.common.FaultType;
import com.aslan.energyconsumptionbackend.controller.FractionController;
import com.aslan.energyconsumptionbackend.entity.Fraction;
import com.aslan.energyconsumptionbackend.service.serviceImpl.FractionServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by aslan.atasoy on 11/18/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(value = FractionController.class, secure = false)
public class FractionTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private FractionServiceImpl fractionService;


    private DataInitializer dataInitializer = new DataInitializer();

    @Test
    public void testGetFraction() throws Exception {
        //mock service with profile B
        Mockito.when(fractionService.getFractionByProfile("B"))
                .thenReturn(new ResponseEntity(dataInitializer.getFraction("B"),HttpStatus.OK));

        //test controller with profile B
        mockMvc.perform(MockMvcRequestBuilders.get("/fraction/{profile}","B").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(12)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].monthProfile.profile").value("B"));
        verify(fractionService, times(1)).getFractionByProfile("B");
        verifyNoMoreInteractions(fractionService);
    }

    @Test
    public void testGetAllFractions() throws Exception {

        Mockito.when(fractionService.getAllFractions())
                .thenReturn(new ResponseEntity(dataInitializer.getFraction("A"), HttpStatus.OK));

        mockMvc.perform(MockMvcRequestBuilders.get("/fraction").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(12)))
                .andExpect(jsonPath("$.[0].monthProfile.profile").value("A"));

    }

    @Test
    public void testUpdateFraction() throws Exception {
        List<Fraction> fractionList = dataInitializer.getFraction("A");
        Mockito.when(fractionService.updateFraction(Mockito.anyList()))
                .thenReturn(new ResponseEntity(fractionList, HttpStatus.OK));

        mockMvc.perform(MockMvcRequestBuilders.put("/fraction")
                .content(dataInitializer.getFractionStringA()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(12)))
                .andExpect(jsonPath("$.[0].monthProfile.profile").value("A"))
                .andReturn();

        verify(fractionService,times(1)).updateFraction(Mockito.anyList());
        verifyNoMoreInteractions(fractionService);
    }

    @Test
    public void testCreateFraction() throws Exception {
        Mockito.when(fractionService.createFraction(Mockito.anyList())).thenReturn(new ResponseEntity(dataInitializer.getFraction("A"), HttpStatus.CREATED));

        mockMvc.perform(MockMvcRequestBuilders.post("/fraction").content(dataInitializer.getFractionStringA())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$",hasSize(12)))
                .andExpect(jsonPath("$.[0].monthProfile.profile").value("A"));
        verify(fractionService,times(1)).createFraction(Mockito.anyList());
        verifyNoMoreInteractions(fractionService);
    }

    @Test
    public void testGetAllFractionWith404()throws Exception{
        FaultType faultType = new FaultType();
        faultType.setFaultMessage("Nothing found");
        faultType.setFaultCode(HttpStatus.NOT_FOUND.toString());

        Mockito.when(fractionService.getAllFractions()).thenReturn(new ResponseEntity(faultType,HttpStatus.NOT_FOUND));

        mockMvc.perform(MockMvcRequestBuilders.get("/fraction"))
                .andExpect(jsonPath("$.faultMessage").value("Nothing found"))
                .andExpect(jsonPath("$.faultCode").value(404))
                .andExpect(status().isNotFound())
                .andReturn();

        verify(fractionService,times(1)).getAllFractions();
        verifyNoMoreInteractions(fractionService);
    }

    @Test
    public void testGetFractionByProfileWith404()throws Exception{
        FaultType fault = new FaultType();
        fault.setFaultMessage("Nothing found with given profile " + "A");
        fault.setFaultCode(HttpStatus.NOT_FOUND.toString());

        Mockito.when(fractionService.getFractionByProfile("A"))
                .thenReturn(new ResponseEntity(fault,HttpStatus.NOT_FOUND));

        mockMvc.perform(MockMvcRequestBuilders.get("/fraction/{profile}","A"))
                .andExpect(jsonPath("$.faultMessage").value("Nothing found with given profile A"))
                .andExpect(jsonPath("$.faultCode").value(404))
                .andExpect(status().isNotFound())
                .andReturn();

        verify(fractionService,times(1)).getFractionByProfile("A");
        verifyNoMoreInteractions(fractionService);
    }

    @Test
    public void testCreateFractionWith409()throws Exception{
        FaultType faultType = new FaultType();
        faultType.setFaultCode(HttpStatus.CONFLICT.toString());
        faultType.setFaultMessage("Data already created before");

        Mockito.when(fractionService.createFraction(Mockito.anyList())).thenReturn(new ResponseEntity(faultType,HttpStatus.CONFLICT));

        mockMvc.perform(MockMvcRequestBuilders.post("/fraction").contentType(MediaType.APPLICATION_JSON)
                .content(dataInitializer.getFractionStringA()))
                .andExpect(jsonPath("$.faultCode").value(409))
                .andExpect(jsonPath("$.faultMessage").value("Data already created before"))
                .andExpect(status().isConflict())
                .andReturn();

        verify(fractionService,times(1)).createFraction(Mockito.anyList());
        verifyNoMoreInteractions(fractionService);

    }
}
