package com.aslan.energyconsumptionbackend.common;

import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfile;
import com.aslan.energyconsumptionbackend.dataSpecs.MonthProfileConnection;
import com.aslan.energyconsumptionbackend.entity.Fraction;
import com.aslan.energyconsumptionbackend.entity.MeterReading;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aslan.atasoy on 11/18/2017.
 */
public class DataInitializer {

    public String getFractionStringA() {
        return "[{\n" +
                "\t\"monthProfile\":{\"month\":\"JAN\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"FEB\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"MAR\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"APR\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"MAY\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"JUN\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"JUL\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"AUG\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"SEP\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"OCT\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"NOV\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"DEC\",\n" +
                "\t\"profile\":\"A\"},\n" +
                "\t\"fractions\":0.05\n" +
                "}\n" +
                "\n" +
                "]";
    }

    public String getFractionStringB() {
        return "[{\n" +
                "\t\"monthProfile\":{\"month\":\"JAN\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"FEB\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"MAR\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"APR\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"MAY\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"JUN\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"JUL\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"AUG\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"SEP\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"OCT\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"NOV\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"DEC\",\n" +
                "\t\"profile\":\"B\"},\n" +
                "\t\"fractions\":0.1\n" +
                "}\n" +
                "\n" +
                "]";


    }

    public String getMeterReadingB(){
        return "[{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"JAN\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":100\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"FEB\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":150\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"MAR\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":200\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"APR\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":300\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"MAY\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":400\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"JUN\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":510\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"JUL\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":600\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"AUG\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":700\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"SEP\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":750\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"OCT\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":800\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"NOV\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":900\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"DEC\",\"profile\":\"B\",\"connection\":1},\n" +
                "\t\"meterReading\":1000\n" +
                "}\n" +
                "\n" +
                "]";
    }

    public String getMeterReadingA(){
        return "[{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"JAN\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":100\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"FEB\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":200\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"MAR\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":300\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"APR\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":400\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"MAY\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":500\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"JUN\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":600\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"JUL\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":700\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"AUG\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":800\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"SEP\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":900\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"OCT\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":1000\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"NOV\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":1050\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"DEC\",\"profile\":\"A\",\"connection\":2},\n" +
                "\t\"meterReading\":1100\n" +
                "}\n" +
                "\n" +
                "]";
    }

    public String getMeterReadingC() {

        return "[{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"JAN\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":100\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"FEB\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":150\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"MAR\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":200\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"APR\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":300\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"MAY\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":400\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"JUN\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":500\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"JUL\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":600\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"AUG\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":650\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"SEP\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":700\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"OCT\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":800\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"NOV\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":900\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfileConnection\":{\"month\":\"DEC\",\"profile\":\"C\",\"connection\":1},\n" +
                "\t\"meterReading\":1000\n" +
                "}\n" +
                "\n" +
                "]";
    }

    public List<Fraction> getFraction(String profile) throws Exception {
        List<Fraction> response = new ArrayList<>();
        JSONArray jsonArray;
        if (profile.equals("B")) {
            jsonArray = new JSONArray(this.getFractionStringB());
        } else if (profile.equals("A")) {
            jsonArray = new JSONArray(this.getFractionStringA());
        } else {
            jsonArray = new JSONArray(this.getFractionStringC());
        }

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject item = jsonArray.getJSONObject(i);
            Fraction fraction = new Fraction();
            fraction.setFractions(BigDecimal.valueOf(item.getDouble("fractions")));
            fraction.setMonthProfile(new MonthProfile(item.getJSONObject("monthProfile").get("month").toString()
                    , item.getJSONObject("monthProfile").get("profile").toString()));
            response.add(fraction);

        }
        return response;
    }

    public List<MeterReading> getMeterReading(String profile) throws JSONException {
        List<MeterReading> response = new ArrayList<>();
        JSONArray jsonArray;
        switch (profile){
            case "A":
                jsonArray = new JSONArray(this.getMeterReadingA());
                break;
            case "B":
                jsonArray = new JSONArray(this.getMeterReadingB());
                break;
            default:
                jsonArray = new JSONArray(this.getMeterReadingC());
                break;
        }

        for(int i = 0; i < jsonArray.length(); i++ ){
            JSONObject item = jsonArray.getJSONObject(i);
            MeterReading meterReading = new MeterReading();
            JSONObject parsedItem = item.getJSONObject("monthProfileConnection");
            meterReading.setMonthProfileConnection(new MonthProfileConnection(parsedItem.get("month").toString(),
                    parsedItem.get("profile").toString(),parsedItem.getLong("connection")));
            meterReading.setMeterReading(item.getInt("meterReading"));
            response.add(meterReading);
        }

        return response;
    }

    private String getFractionStringC() {
        return "[{\n" +
                "\t\"monthProfile\":{\"month\":\"JAN\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"FEB\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"MAR\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"APR\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"MAY\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"JUN\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"JUL\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"AUG\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"SEP\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"OCT\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.05\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"NOV\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.1\n" +
                "},\n" +
                "{\n" +
                "\t\"monthProfile\":{\"month\":\"DEC\",\n" +
                "\t\"profile\":\"C\"},\n" +
                "\t\"fractions\":0.1\n" +
                "}\n" +
                "\n" +
                "]";
    }


}
